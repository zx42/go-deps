package zap

import (
	"os"
	"regexp"

	"go.uber.org/zap"
)

type (
	zapLoggerT struct {
		sugar  *zap.SugaredLogger
		regExL []*regexp.Regexp
	}
)

// var (
// 	defaultLogger = NewLogger()
// )

// func init() {
// 	log.RegisterLogger(defaultLogger)
// }

func NewLogger() *zapLoggerT {
	return newLogger(false)

	// // logger, err := zap.NewProduction(zap.AddCallerSkip(1))
	// logger, err := zap.NewDevelopment(zap.AddCallerSkip(1))
	// if err != nil {
	// 	panic(err)
	// }
	// // defer logger.Sync() // flushes buffer, if any

	// // func AddCallerSkip

	// zl := &zapLoggerT{
	// 	// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
	// 	sugar: logger.Sugar(),
	// }
	// return zl
}

func NewDebugLogger(regExSL ...string) *zapLoggerT {
	return newLogger(true, regExSL...)

	// // logger, err := zap.NewProduction(zap.AddCallerSkip(1))
	// logger, err := zap.NewDevelopment(zap.AddCallerSkip(1))
	// if err != nil {
	// 	panic(err)
	// }
	// // defer logger.Sync() // flushes buffer, if any

	// // func AddCallerSkip

	// zl := &zapLoggerT{
	// 	// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
	// 	sugar: logger.Sugar(),
	// }
	// return zl
}

func newLogger(dbg bool, regExSL ...string) *zapLoggerT {
	var (
		logger *zap.Logger
		err    error
	)

	if dbg {
		logger, err = zap.NewDevelopment(zap.AddCallerSkip(1))
	} else {
		logger, err = zap.NewProduction(zap.AddCallerSkip(1))
	}
	if err != nil {
		panic(err)
	}
	// defer logger.Sync() // flushes buffer, if any

	// func AddCallerSkip

	zl := &zapLoggerT{
		// sugar: logger.WithOptions(zap.AddCallerSkip(1)).Sugar(),
		sugar: logger.Sugar(),
	}
	for _, rxS := range regExSL {
		rx := regexp.MustCompile(rxS)
		zl.regExL = append(zl.regExL, rx)
	}
	return zl
}

func (zl *zapLoggerT) Error(msg string, args ...interface{}) {
	zl.sugar.Errorw(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}

func (zl *zapLoggerT) Info(msg string, args ...interface{}) {
	zl.sugar.Infow(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}

func (zl *zapLoggerT) Debug(msg string, args ...interface{}) {
	if !zl.argMatches(append(args, msg)) {
		return
	}

	zl.sugar.Debugw(msg, args...)

	// err := zl.sugar.Sync()
	// if err != nil {
	// 	panic(err)
	// }
}

func (zl *zapLoggerT) argMatches(args []interface{}) bool {
	if len(zl.regExL) == 0 {
		return true
	}

	for _, arg := range args {
		argS, ok := arg.(string)
		if ok {
			for _, rx := range zl.regExL {
				if rx.MatchString(argS) {
					return true
				}
			}
		}
	}
	return false
}

func (zl *zapLoggerT) Fatal(errStatus int, msg string, args ...interface{}) {
	zl.Info(msg, args...)
	os.Exit(errStatus)
}
