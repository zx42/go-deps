package log

import (
	"gitlab.com/zx42/go-deps/log/zap"
)

type (
	Logger interface {
		Error(msg string, args ...interface{})
		Info(msg string, args ...interface{})
		Debug(msg string, args ...interface{})
		Fatal(errStatus int, msg string, args ...interface{})
	}
)

// var (
// 	defaultLogger Logger = zap.NewLogger()
// 	// debugLogger   Logger = zap.NewDebugLogger()
// )

func GetLogger() Logger {
	return zap.NewLogger()
	// return GetDebugLogger()
	// // return defaultLogger
}

func GetDebugLogger(regExSL ...string) Logger {
	// return debugLogger
	return zap.NewDebugLogger(regExSL...)
}

// func RegisterLogger(lg Logger) {
// 	defaultLogger = lg
// }
